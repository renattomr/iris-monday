module go-iris-mongo

go 1.15

require (
	contrib.go.opencensus.io/exporter/zipkin v0.1.2
	github.com/Joker/hpp v1.0.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/kataras/iris/v12 v12.2.0-alpha2.0.20210217145719-f34703e3cffb
	github.com/machinebox/graphql v0.2.2
	github.com/matryer/is v1.4.0 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/openzipkin/zipkin-go v0.2.5
	github.com/prometheus/client_golang v1.9.0
	github.com/yudai/pp v2.0.1+incompatible // indirect
	go.mongodb.org/mongo-driver v1.4.6
	go.opencensus.io v0.22.6
)
