package controllers

import (
	"go-iris-mongo/models"
	"go-iris-mongo/querys"
	"log"
	"strconv"
	"strings"

	"github.com/kataras/iris/v12"
	"go.opencensus.io/trace"
)

var Monday models.MondayClient

func GetAllUsers(ctx iris.Context) {
	log.Println(ctx.Request().Context())
	_, span := trace.StartSpan(ctx.Request().Context(), "users")
	defer span.End()
	users, err := querys.GetUsers(Monday.Client)
	if err != nil {
		ctx.Application().Logger().Errorf("%v", err)
		return
	} else {
		ctx.Application().Logger().Infof("%v", users)
	}
	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(users)
	ctx.Next()
}

func GetAllBoards(ctx iris.Context) {
	_, span := trace.StartSpan(ctx.Request().Context(), "boards")
	defer span.End()
	boards, err := querys.GetBoards(Monday.Client)
	if err != nil {
		ctx.Application().Logger().Errorf("%v", err)
		return
	} else {
		ctx.Application().Logger().Infof("%v", boards)
	}
	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(boards)
}

func GetAllColumns(ctx iris.Context) {
	_, span := trace.StartSpan(ctx.Request().Context(), "columns")
	defer span.End()
	id := ctx.Params().Get("id")
	idInt, err := strconv.Atoi(id)
	if err != nil {
		ctx.Application().Logger().Errorf("%v", err)
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(iris.Map{
			"error": "Incorrect id format",
		})
		return
	} else {
		ctx.Application().Logger().Infof("%v", idInt)
	}
	colums, erro := querys.GetColumns(Monday.Client, idInt)

	if erro != nil {
		ctx.Application().Logger().Errorf("%v", erro)
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(iris.Map{
			"error": "Incorrect monday client",
		})
		return
	} else {
		ctx.Application().Logger().Infof("%v", colums)
	}
	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(colums)
}

func TransformMondayText(ctx iris.Context) {
	_, span := trace.StartSpan(ctx.Request().Context(), "text")
	defer span.End()
	var j models.Res
	err := ctx.ReadJSON(&j)
	if err != nil {
		ctx.Application().Logger().Errorf("%v", err)
		return
	}

	boardId := j.Payload.InputFields.BoardId
	itemId := j.Payload.InputFields.ItemId
	sourceColumnId := j.Payload.InputFields.SourceColumnId
	targetColumnId := j.Payload.InputFields.TargetColumnId

	value, erro := querys.GetColumnValue(Monday.Client, itemId, sourceColumnId)
	if erro != nil {
		ctx.Application().Logger().Errorf("%v", err)
		return
	}
	upperCase := strings.ToUpper(value)
	erroo := querys.ChangeColumnValue(Monday.Client, boardId, itemId, targetColumnId, upperCase)
	if erroo != nil {
		ctx.Application().Logger().Errorf("%v", erroo)
	}
	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(iris.Map{
		"message": "success",
	})

}

func StatusUpdated(ctx iris.Context) {
	_, span := trace.StartSpan(ctx.Request().Context(), "status")
	defer span.End()
	var p map[string]interface{}
	err := ctx.ReadJSON(&p)
	if err != nil {
		ctx.Application().Logger().Errorf("%v", err)
		return
	} else {
		ctx.Application().Logger().Infof("%v", p)
	}
}
