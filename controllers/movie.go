package controllers

import (
	"go-iris-mongo/database"
	"go-iris-mongo/models"

	"github.com/kataras/iris/v12"
	"go.opencensus.io/trace"
)

var Movies database.MongoCollection

func GetMovies(ctx iris.Context) {
	_, span := trace.StartSpan(ctx.Request().Context(), "getmovies")
	defer span.End()
	movies, err := Movies.GetAll(nil)
	if err != nil {
		ctx.Application().Logger().Errorf("Error getting All Movies: %v", err)
		return
	}

	if movies == nil {
		movies = make([]models.Movie, 0)
	}
	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(movies)

}

func AddMovie(ctx iris.Context) {
	obj := new(models.Movie)
	err := ctx.ReadJSON(obj)

	if err != nil {
		ctx.Application().Logger().Errorf("Error LogJSON: %v", err)
		return
	} else {
		ctx.Application().Logger().Infof("%v", obj)
	}

	err = Movies.Create(nil, obj)
	if err != nil {
		ctx.Application().Logger().Errorf("Error DB: %v", err)
		return
	}

	ctx.StatusCode(iris.StatusCreated)
	ctx.JSON(obj)
}

func GetMovieById(ctx iris.Context) {
	id := ctx.Params().Get("id")

	movie, err := Movies.GetByID(nil, id)
	if err != nil {
		ctx.Application().Logger().Errorf("Error getting the movie: %v", err)
		return
	} else {
		ctx.Application().Logger().Infof("Movie retrieved: %v", movie)
	}

	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(movie)
}

func UpdateMovieById(ctx iris.Context) {
	id := ctx.Params().Get("id")

	var m models.Movie

	err := ctx.ReadJSON(&m)

	if err != nil {
		ctx.Application().Logger().Errorf("%v", err)
		return
	} else {
		ctx.Application().Logger().Infof("%v", m)
	}

	err = Movies.Update(nil, id, m)

	if err != nil {
		ctx.Application().Logger().Errorf("%v", err)
		return
	}
	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(iris.Map{"id": id, "status": "updated"})

}

func DeleteMovie(ctx iris.Context) {
	id := ctx.Params().Get("id")

	err := Movies.Delete(nil, id)

	if err != nil {
		ctx.Application().Logger().Errorf("%v", err)
		return
	}
	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(iris.Map{"id": id, "status": "deleted"})
}
