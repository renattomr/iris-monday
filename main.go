package main

import (
	"context"
	"go-iris-mongo/controllers"
	"go-iris-mongo/database"
	"go-iris-mongo/models"
	"log"
	"math/rand"
	"net/http"

	"os"
	"time"

	"contrib.go.opencensus.io/exporter/zipkin"
	"github.com/joho/godotenv"
	"github.com/kataras/iris/v12"
	"github.com/machinebox/graphql"
	openzipkin "github.com/openzipkin/zipkin-go"
	zipkinHTTP "github.com/openzipkin/zipkin-go/reporter/http"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.opencensus.io/plugin/ochttp"
	"go.opencensus.io/stats/view"
	"go.opencensus.io/trace"
)

func registerZipkin() {
	localEndpoint, err := openzipkin.NewEndpoint("goDoktuzApi", "192.168.1.3:3000")
	if err != nil {
		log.Fatalf("Failed to create Zipkin exporter: %v", err)
	}
	reporter := zipkinHTTP.NewReporter("https://zipkin.doktuz.com/api/v2/spans")
	exporter := zipkin.NewExporter(reporter, localEndpoint)
	trace.RegisterExporter(exporter)
	trace.ApplyConfig(trace.Config{DefaultSampler: trace.AlwaysSample()})
}

var (
	counter = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "golang",
			Name:      "my_counter",
			Help:      "This is my counter",
		})

	gauge = prometheus.NewGauge(
		prometheus.GaugeOpts{
			Namespace: "golang",
			Name:      "my_gauge",
			Help:      "This is my gauge",
		})

	histogram = prometheus.NewHistogram(
		prometheus.HistogramOpts{
			Namespace: "golang",
			Name:      "my_histogram",
			Help:      "This is my histogram",
		})

	summary = prometheus.NewSummary(
		prometheus.SummaryOpts{
			Namespace: "golang",
			Name:      "my_summary",
			Help:      "This is my summary",
		})
)

// CreateClient creates a graphql client (safe to share across requests)
func CreateClient() *graphql.Client {
	return graphql.NewClient("https://api.monday.com/v2/")
}

var Monday *models.MondayClient = &controllers.Monday
var Movies *database.MongoCollection = &controllers.Movies

func main() {
	registerZipkin()
	rand.Seed(time.Now().Unix())

	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error Loading .env file")
	}
	mongoURI := os.Getenv("MONGO_URI")
	app := iris.New()
	Monday.Client = CreateClient()
	app.Handle("GET", "/metrics", iris.FromStd(promhttp.Handler()))
	prometheus.MustRegister(counter)
	prometheus.MustRegister(gauge)
	prometheus.MustRegister(histogram)
	prometheus.MustRegister(summary)
	app.Get("/index", index)
	v1 := app.Party("api/v1")
	{
		v1.Get("/users", controllers.GetAllUsers)
		v1.Get("/boards", controllers.GetAllBoards)
		v1.Get("/columns/{id}", controllers.GetAllColumns)
		v1.Post("/transformation/transform", controllers.TransformMondayText)
		v1.Post("/column/status", controllers.StatusUpdated)
		v1.Post("/movies", controllers.AddMovie)
		v1.Get("/movies", controllers.GetMovies)
		v1.Get("/movies/{id}", controllers.GetMovieById)
		v1.Put("/movies/{id}", controllers.UpdateMovieById)
		v1.Delete("/movies/{id}", controllers.DeleteMovie)
	}
	ctx, cancel := context.WithTimeout(context.Background(), 200*time.Millisecond)
	defer cancel()
	//ctx := context.Background()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(mongoURI))
	defer func() {
		if err = client.Disconnect(ctx); err != nil {
			panic(err)
		}
	}()
	db := client.Database("MoviesApp")
	collection := db.Collection("Movies")
	Movies.C = collection
	h := &ochttp.Handler{Handler: app}
	if err := view.Register(ochttp.DefaultServerViews...); err != nil {
		log.Fatal("Failed to register ochttp.DefaultServerViews")
	}
	srv := &http.Server{Addr: ":3000", Handler: h}
	app.Run(iris.Raw(srv.ListenAndServe), iris.WithoutBodyConsumptionOnUnmarshal)

	// app.Run(iris.TLS("127.0.0.1:443", "Certificate.crt", "Private.key"), iris.WithoutBodyConsumptionOnUnmarshal)
	// app.Listen(":3000", iris.WithoutBodyConsumptionOnUnmarshal)
}

func index(ctx iris.Context) {
	prueba(ctx.Request().Context())

	_, span := trace.StartSpan(ctx.Request().Context(), "index")
	defer span.End()
	time.Sleep(800 * time.Millisecond)
	ctx.StatusCode(iris.StatusOK)
	ctx.JSON(iris.Map{
		"hello": "world",
	})
}
func prueba(ctx context.Context) {
	_, span := trace.StartSpan(ctx, "database")
	defer span.End()
	time.Sleep(500 * time.Millisecond)
}
