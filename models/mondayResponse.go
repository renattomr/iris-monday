package models

type Fields struct {
	BoardId        int    `json:"boardId"`
	ItemId         int    `json:"itemId"`
	SourceColumnId string `json:"sourceColumnId"`
	TargetColumnId string `json:"targetColumnId"`
}

type Payload struct {
	InputFields Fields `json:"inputFields"`
}

type Res struct {
	Payload Payload `json:"payload"`
}
