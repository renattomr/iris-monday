package models

import "github.com/machinebox/graphql"

type MondayClient struct {
	Client *graphql.Client
}

type User struct {
	Id    int    `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
}

type Column struct {
	Id       string `json:"id"`
	Title    string `json:"title"`
	Type     string `json:"type"`         // text, boolean, color, ...
	Settings string `json:"settings_str"` // used to get label index values for color(status) and dropdown column types
}

type Board struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}
