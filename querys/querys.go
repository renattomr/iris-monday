package querys

import (
	"context"
	"go-iris-mongo/models"
	"log"

	"github.com/machinebox/graphql"
)

const token = "eyJhbGciOiJIUzI1NiJ9.eyJ0aWQiOjEwMDM5NDE0NCwidWlkIjoyMDQxNzMwOCwiaWFkIjoiMjAyMS0wMi0yMFQyMzozMzowNi4wMDBaIiwicGVyIjoibWU6d3JpdGUiLCJhY3RpZCI6ODI5NzAxNywicmduIjoidXNlMSJ9.gBu98AiNUh7iNdpLhtEktp-0TKL24yDPqkqvF6uMAJA"

// RunRequest executes request and decodes response into response parm (address of object)
func RunRequest(client *graphql.Client, req *graphql.Request, response interface{}) error {
	req.Header.Set("Cache-Control", "no-cache")
	req.Header.Set("Authorization", token)
	req.Header.Set("Content-Type", "application/json")
	ctx := context.Background()
	err := client.Run(ctx, req, response)
	return err
}

// GetUsers returns []User for all users.
func GetUsers(client *graphql.Client) ([]models.User, error) {
	req := graphql.NewRequest(`
	    query {
            users {
                id name email
            }
        }
    `)
	var response struct {
		Users []models.User `json:"users"`
	}
	err := RunRequest(client, req, &response)
	return response.Users, err
}

// GetColumns returns []Column for specified boardId.
func GetColumns(client *graphql.Client, boardId int) ([]models.Column, error) {
	req := graphql.NewRequest(`
	    query ($boardId: [Int]) {
            boards (ids: $boardId) {
                columns {id title type settings_str}
            }
        }
    `)
	req.Var("boardId", []int{boardId})
	type board struct {
		Columns []models.Column `json:"columns"`
	}
	var response struct {
		Boards []board `json:"boards"`
	}
	err := RunRequest(client, req, &response)
	return response.Boards[0].Columns, err
}

// GetBoards returns []Board for all boards.
func GetBoards(client *graphql.Client) ([]models.Board, error) {
	req := graphql.NewRequest(`
	    query {
            boards {
                id name
            }
        }
    `)
	var response struct {
		Boards []models.Board `json:"boards"`
	}
	err := RunRequest(client, req, &response)
	return response.Boards, err
}

func GetColumnValue(client *graphql.Client, itemId int, columnId string) (string, error) {
	req := graphql.NewRequest(`
	query($itemId: [Int], $columnId: [String]) {
        items (ids: $itemId) {
          column_values(ids:$columnId) {
            value
          }
        }
      }
	`)
	req.Var("itemId", itemId)
	req.Var("columnId", columnId)

	var response map[string]interface{}
	err := RunRequest(client, req, &response)
	if err != nil {
		log.Println(err)
		return "", err
	}
	items := response["items"].([]interface{})
	i := items[0].(map[string]interface{})
	j := i["column_values"].([]interface{})
	value := ((j[0].(map[string]interface{}))["value"]).(string)
	return value, nil
}

func ChangeColumnValue(client *graphql.Client, boardId int, itemId int, columnId string, value string) error {
	req := graphql.NewRequest(`
	mutation change_column_value($boardId: Int!, $itemId: Int!, $columnId: String!, $value: JSON!) {
        change_column_value(board_id: $boardId, item_id: $itemId, column_id: $columnId, value: $value) {
          id
        }
      }
	`)

	req.Var("boardId", boardId)
	req.Var("itemId", itemId)
	req.Var("columnId", columnId)
	req.Var("value", value)

	var response interface{}
	err := RunRequest(client, req, &response)
	if err != nil {
		return err
	}
	return nil
}
