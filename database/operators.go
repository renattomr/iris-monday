package database

import (
	"context"
	"errors"
	"go-iris-mongo/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type Service interface {
	Create(ctx context.Context, m *models.Movie) error
	GetAll(ctx context.Context) ([]models.Movie, error)
	GetByID(ctx context.Context, id string) (models.Movie, error)
	Update(ctx context.Context, id string, m models.Movie) error
	Delete(ctx context.Context, id string) error
}

type MongoCollection struct {
	C *mongo.Collection
}

func (s *MongoCollection) Create(ctx context.Context, m *models.Movie) error {
	if m.ID.IsZero() {
		m.ID = primitive.NewObjectID()
	}

	_, err := s.C.InsertOne(ctx, m)
	if err != nil {
		return err
	}

	return nil
}

func (s *MongoCollection) GetAll(ctx context.Context) ([]models.Movie, error) {

	cur, err := s.C.Find(ctx, bson.D{})
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)

	var results []models.Movie

	for cur.Next(ctx) {
		if err = cur.Err(); err != nil {
			return nil, err
		}
		var elem models.Movie
		err = cur.Decode(&elem)

		if err != nil {
			return nil, err
		}
		results = append(results, elem)
	}

	return results, nil
}

func matchID(id string) (bson.D, error) {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	filter := bson.D{{Key: "_id", Value: objectID}}
	return filter, nil
}

func (s *MongoCollection) GetByID(ctx context.Context, id string) (models.Movie, error) {
	var movie models.Movie
	filter, err := matchID(id)
	if err != nil {
		return movie, err
	}

	err = s.C.FindOne(ctx, filter).Decode(&movie)
	if err == mongo.ErrNoDocuments {
		return movie, errors.New("Movie not found")
	}
	return movie, err
}

func (s *MongoCollection) Update(ctx context.Context, id string, movie models.Movie) error {
	filter, err := matchID(id)
	if err != nil {
		return err
	}

	elem := bson.D{}

	if movie.Name != "" {
		elem = append(elem, bson.E{Key: "name", Value: movie.Name})
	}

	if movie.Director != "" {
		elem = append(elem, bson.E{Key: "director", Value: movie.Director})
	}

	if movie.Description != "" {
		elem = append(elem, bson.E{Key: "description", Value: movie.Description})
	}

	update := bson.D{
		{Key: "$set", Value: elem},
	}

	_, erro := s.C.UpdateOne(ctx, filter, update)

	if erro != nil {
		if err == mongo.ErrNoDocuments {
			return errors.New("Movie not updated")
		}
	}

	return erro
}

func (s *MongoCollection) Delete(ctx context.Context, id string) error {
	filter, err := matchID(id)

	if err != nil {
		return err
	}

	_, err = s.C.DeleteOne(ctx, filter)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return errors.New("Document not found")
		}
		return err
	}

	return nil
}
